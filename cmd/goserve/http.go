package main

import (
	"bufio"
	"github.com/jehiah/go-strftime"
	"io"
	"net"
	"os"
	"strings"
	"time"
)

type HTTPServer struct{}

type noBody struct{}

func (noBody) Write(p []byte) (n int, err error) { return 0, nil }
func (noBody) Read([]byte) (int, error)          { return 0, io.EOF }

var NoBody io.ReadWriter = noBody{}

func (t *HTTPServer) HandleConnection(ctx Context) {
	conn := ctx.conn
	var tcpConn = conn.(*net.TCPConn)

	_ = tcpConn.SetKeepAlive(true)
	_ = tcpConn.SetKeepAlivePeriod(globalConfig.IdleTimeout)

	defer func() {
		logMain.Debug("Closing connection.")
		_ = conn.Close()
	}()

	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)

	userIdentifier := "-"

	_ = conn.SetReadDeadline(time.Now().Add(globalConfig.ReadTimeout))
	os.Setenv(REMOTE_ADDR, conn.RemoteAddr().String())
	for {
		request, err := readRequest(reader)
		_ = conn.SetReadDeadline(time.Now().Add(globalConfig.ReadTimeout))

		header := make(Header)
		header.Set(HeaderDate, FormatTime(time.Now()))
		header.Set(HeaderServer, globalConfig.ServerInfo)
		header.Set(HeaderContentLength, "0")

		closeConnection := false

		var response *Response

		if err != nil {
			logMain.Debug(err.Error())
			switch {
			case isConnectionClosed(err):
				header.Set(HeaderConnection, HeaderConnectionClose)
				response = createErrorResponse(StatusRequestTimeout, header, request)
				closeConnection = true
			case isBadRequestError(err):
				response = createErrorResponse(StatusBadRequest, header, request)
			case isInvalidMethod(err):
				response = createErrorResponse(StatusNotImplemented, header, request)
			case isUnsupportedVersionError(err):
				response = createErrorResponse(StatusHTTPVersionNotSupported, header, request)
			default:
				response = createErrorResponse(StatusInternalServerError, header, request)
			}
		}

		if request != nil && response == nil {
			response = createResponse(StatusNotFound, header, request, nil, 0)

			handler := ctx.host.multiplexer
			handler.Handle(response, request)

			if !request.ShouldContinueConnection() {
				closeConnection = true
			}

			ip := strings.Split(conn.RemoteAddr().String(), ":")[0]

			referrer, ok := request.Header.Get(HeaderReferer)
			if !ok {
				referrer = "-"
			}

			agent, ok := request.Header.Get(HeaderUserAgent)
			if !ok {
				agent = "-"
			}
			logAccess.Infof("%s %s %s [%s] \"%s %s %s\" %d %d %s %s",
				ip,
				userIdentifier,
				request.User,
				strftime.Format("%d/%b/%Y:%H:%M:%S %z", time.Now()),
				request.Method,
				request.RequestURI,
				request.Protocol,
				response.StatusCode,
				response.ContentLength,
				referrer,
				agent,
			)

			if response.StatusCode == StatusNotFound {
				response = createErrorResponse(StatusNotFound, header, request)
			}
		}

		if response != nil {
			if v, _ := response.Header.Get(HeaderTransferEncoding); v != HeaderTransferEncodingChunked {
				header.Set(HeaderAcceptRange, HeaderAcceptRangeBytes)
			}

			_ = conn.SetWriteDeadline(time.Now().Add(globalConfig.WriteTimeout))

			err = response.Write(writer)
			if err != nil {
				logMain.Error(err)
			}
		}

		if closeConnection {
			return
		}
	}
}
