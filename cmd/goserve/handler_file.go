package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type Dir string

func (d Dir) Root() string {
	return filepath.Clean(string(d))
}

func (d Dir) GetFullPath(name string) string {
	//Correct empty directory to . for current directory
	dir := d.Root()
	if dir == "" {
		dir = "."
	}
	fullPath := filepath.Join(dir, filepath.FromSlash(path.Clean("/"+name)))
	return fullPath
}

func (d Dir) open(realName string) (File, error) {
	f, err := os.Open(realName)
	if err != nil {
		return nil, err
	}
	return f, nil
}

func (d Dir) Open(name string) (File, error) {
	if filepath.Separator != '/' && strings.ContainsRune(name, filepath.Separator) {
		return nil, errors.New("invalid character in file path")
	}
	fullPath := d.GetFullPath(name)
	return d.open(fullPath)
}

func (d Dir) Walk(root string, walkFunc WalkFunc) error {
	fullPath := d.GetFullPath(root)
	return filepath.Walk(fullPath, filepath.WalkFunc(walkFunc))
}

func (d Dir) List(relPath string) ([]os.FileInfo, error) {
	file, err := d.Open(relPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	return file.Readdir(0)
}
func (d Dir) IsDir(relPath string) (bool, error) {
	//oof
	file, err := d.Open(relPath)
	if err != nil {
		return false, err
	}
	stat, err := file.Stat()
	if err != nil {
		return false, err
	}
	return stat.IsDir(), nil
}

type ReadCloseSeeker interface {
	io.Closer
	io.Reader
	io.Seeker
}

type File interface {
	io.Closer
	io.Reader
	io.Seeker
	Readdir(count int) ([]os.FileInfo, error)
	Stat() (os.FileInfo, error)
}

type WalkFunc func(path string, info os.FileInfo, err error) error

type FileSystem interface {
	Open(name string) (File, error)
	GetFullPath(name string) string
	Walk(relPath string, walkFunc WalkFunc) error
	List(relPath string) ([]os.FileInfo, error)
	Root() string
	IsDir(relPath string) (bool, error)
}

type FileHandler struct {
	root  FileSystem
	index string
}

func FileServer(root FileSystem, index string) *FileHandler {
	return &FileHandler{root, index}
}

func (h *FileHandler) Handle(ctx *handlerContext, response *Response, request *Request) {
	upath := request.URL.Path
	logMain.Debug("URL Path ", upath)
	serveFile(response, request, h.root, path.Clean(upath), h.index)
}

func endsWith(string string, c rune) bool {
	return rune(string[len(string)-1]) == c
}

type closableBuffer struct {
	bytes.Buffer
}

func (closableBuffer) Close() error { return nil }

func (closableBuffer) Seek(offset int64, whence int) (int64, error) { return 0, nil }

func serveFile(response *Response, request *Request, fs FileSystem, name string, indexPage string) {
	//Use filesystem abstraction to open file
	file, err := fs.Open(name)
	if err != nil {
		var err error
		switch request.Method {
		case MethodGet, MethodHead:
			determineContentNegotiation(response, request, fs, name)
		case MethodPut:
			err = putFile(response, request, fs, name)
		case MethodDelete:
			updateErrorResponse(response, StatusNotFound)
			response.SetStatus(StatusOK)
		}
		logMain.Error(err)

		return
	}

	defer file.Close()

	if request.Method == MethodDelete {
		err = deleteFile(response, request, fs, name)
		logMain.Error(err)
		return
	}

	stat, err := file.Stat()
	if err != nil {
		logMain.Error(err)
		return
	}
	url := request.URL.Path
	if stat.IsDir() {
		logMain.Debug(name, " is a directory")
		if !endsWith(url, '/') {
			localRedirect(response, request, strings.TrimPrefix(url, "/")+"/")
			return
		}
	} else {
		if endsWith(url, '/') {
			localRedirect(response, request, "../"+path.Base(url))
			return
		}
	}

	url = request.URL.Path
	if stat.IsDir() {
		index := name + "/" + indexPage
		indexFile, err := fs.Open(index)
		// path/index.html successfully existed
		if err == nil {
			defer indexFile.Close()

			indexStat, err := indexFile.Stat()
			if err == nil {
				//Overwrite current file with index page
				name = index
				stat = indexStat
				file = indexFile
			}
		}
	}

	//Index wasn't found, return directory listing
	if stat.IsDir() {
		directoryListing(response, request, name, fs.GetFullPath(name), stat)
		return
	}

	if err := serveCGI(response, request, fs, name, stat); err == nil {
		return
	}

	if request.Method != MethodOptions {
		//Set ETag
		etag := strongETag(stat)
		response.Header.Set(HeaderETag, etag)
	}

	sizeFunc := func() (int64, error) { return stat.Size(), nil }
	serveContent(response, request, name, stat.ModTime(), sizeFunc, file)
}

type SizeFunc func() (int64, error)

func serveContent(response *Response, request *Request, name string, modtime time.Time, sizeFunc SizeFunc, content ReadCloseSeeker) {
	code := StatusOK
	done := checkPreconditions(response, request, modtime)

	if done {
		return
	}

	fileMeta := determineFileMetadata(name, sizeFunc)
	logMain.Info(fileMeta)
	//Determine Content-Type
	contentTypes, ok := response.Header[HeaderContentType]
	var contentType string
	if !ok {
		contentType := fileMeta.Get("type")
		if contentType == "" {
			contentType = ApplicationOctetStream
		}
		if fileMeta.Get("charset") != "" {
			contentType = fmt.Sprintf("%s; charset=%s", contentType, fileMeta.Get("charset"))
		}
		response.Header.Set(HeaderContentType, contentType)
	} else if len(contentTypes) > 0 {
		contentType = contentTypes[0] //Only need the mime type
	}

	if language := fileMeta.Get("language"); language != "" {
		response.Header.Set(HeaderContentLanguage, language)
	}
	if encoding := fileMeta.Get("encoding"); encoding != "" {
		response.Header.Set(HeaderContentEncoding, encoding)
	}

	logMain.Debug(contentType)

	response.SetStatus(code)

	var err error
	response.ContentLength, err = sizeFunc()
	if err != nil {
		logMain.Error(err)
	}
	response.Header.Set(HeaderLastModified, FormatTime(modtime))
	body, err := readBody(response, request, content)
	if err != nil {
		logMain.Error("handler_file body:", err)
	}
	switch {
	case request.Method == MethodHead:
		response.Body = nil
		break
	case request.Method == MethodOptions:
		response.Header.Set(HeaderAllow, MethodGet, MethodOptions, MethodHead, MethodTrace)
		break
	default:
		response.Body = &body
	}

}

func readBody(response *Response, request *Request, content ReadCloseSeeker) (body io.ReadWriter, err error) {
	var buf []byte
	if rangeHeader, ok := request.Header.Get(HeaderRange); ok {
		rangeSet, err := ParseRange(rangeHeader)
		if err != nil {
			return nil, err
		}
		length := response.ContentLength
		buf, err = rangeSet.Read(content, length)
		rangeSet.SetContentRangeHeader(response)
		if err != nil {
			if isUnsatisfiableRangeSet(err) {
				updateErrorResponse(response, StatusRequestedRangeNotSatisfiable)
			}
			return nil, err
		}
		response.SetStatus(StatusPartialContent)
		response.ContentLength = int64(len(buf))
	} else {
		buf = make([]byte, response.ContentLength)
		_, err = content.Read(buf)
		if err != nil {
			return nil, err
		}
	}
	_ = content.Close()
	return bytes.NewBuffer(buf), nil
}

func deleteFile(response *Response, request *Request, fs FileSystem, name string) error {
	f := fs.GetFullPath(name)
	if _, err := os.Open(f); !os.IsNotExist(err) { //File exists
		err = os.Remove(f)
		if err == nil {
			updateErrorResponse(response, StatusOK)
			return nil
		}
		return err
	}
	return nil
}

func putFile(response *Response, request *Request, fs FileSystem, name string) error {

	if request.ContentLength == 0 {
		updateErrorResponse(response, StatusNoContent)
		response.SetStatus(StatusOK)
		return nil
	}

	var body = *request.Body
	b := make([]byte, request.ContentLength)
	_, err := body.Read(b)
	if err != nil {
		return err
	}
	created, err := writeFile(fs.GetFullPath(name), b)
	if err != nil {
		return err
	}

	if created {
		updateErrorResponse(response, StatusCreated)
	} else {
		updateErrorResponse(response, StatusOK)
	}

	return nil
}

func writeFile(filepath string, data []byte) (bool, error) {
	file, err := os.Open(filepath)
	created := false
	if os.IsNotExist(err) {
		file, err = os.Create(filepath)
		if err != nil {
			return false, err
		}
		created = true
	}
	defer file.Close()
	_, err = io.WriteString(file, string(data))
	if err != nil {
		return false, err
	}

	return created, file.Sync()
}

func localRedirect(response *Response, request *Request, newPath string) {
	//Append query to end of new path
	if q := request.URL.RawQuery; q != "" {
		newPath += "?" + q
	}
	response.Header.Set(HeaderLocation, "/"+newPath)
	response.SetStatus(StatusMovedPermanently)
}

func determineFileMetadata(name string, size SizeFunc) fileMetadata {
	parts := strings.Split(name, ".")

	f := fileMetadata{"name": name}

	f.Set("basename", filepath.Base(name))

	s, err := size()
	if err != nil {
		logMain.Error(err)
	}
	f.Set("length", strconv.FormatInt(s, 10))

	for _, part := range parts {
		logMain.Info(f.Get("basename"), ":", part)

		mime := GetMimeType(part)
		if mime != ApplicationOctetStream {
			f.Set("extension", part)
			f.Set("type", mime)
			continue
		}

		if isValidLanguage(part) {
			f.Set("language", part)
			continue
		}
		if charset, ok := GetCharset(part); ok {
			f.Set("charset", charset)
			continue
		}
		if encoding, ok := GetEncoding(part); ok {
			f.Set("encoding", encoding)
			continue
		}

	}

	return f
}

func (f fileMetadata) Set(k, v string) {
	f[k] = v
}

func (f fileMetadata) Get(k string) string {
	if v, ok := f[k]; ok {
		return v
	}
	return ""
}

func specString(key, value string) string {
	return fmt.Sprintf("{ %s %s }", key, value)
}

func (f fileMetadata) Marshal() string {
	specs := make([]string, 0)
	for k, v := range f {
		if k == "name" || k == "basename" || k == "extension" || v == "" {
			continue
		}
		specs = append(specs, specString(k, v))
	}
	sourceQuality := 1
	return fmt.Sprintf("{ \"%s\" %d %s}", f.Get("basename"), sourceQuality, strings.Join(specs, " "))
}

type fileMetadata map[string]string
