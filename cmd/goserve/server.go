package main

import (
	"errors"
	"fmt"
	"net"
	"os"
	"path/filepath"
	"strconv"
	"sync"
	"time"
)

type Port int16

type Timeout struct {
	ReadTimeout  time.Duration `mapstructure:"read_timeout"`
	WriteTimeout time.Duration `mapstructure:"write_timeout"`
	IdleTimeout  time.Duration `mapstructure:"idle_timeout"`
}

type Host struct {
	ServerName   string                   `mapstructure:"server_name"`
	Address      string                   `mapstructure:"address"`
	Port         Port                     `mapstructure:"port"`
	Type         string                   `mapstructure:"type"`
	DocumentRoot string                   `mapstructure:"root"`
	Locations    []map[string]interface{} `mapstructure:"locations"`
	Timeouts     Timeout                  `mapstructure:"timeouts"`
	AuthHandler  AccessHandler            `mapstructure:"auth_handler"`

	handler     ProtocolHandler
	multiplexer Multiplexer
	listener    *net.TCPListener

	connections chan net.Conn
}

type HostList struct {
	Hosts []*Host `mapstructure:"hosts"`
}

func (host *Host) Listen() {
	var handlers sync.WaitGroup
	for {
		if host.listener == nil {
			logMain.Error(errors.New("Invalid host listener:" + host.ListeningAddress()))
			return
		}
		conn, err := host.listener.Accept()
		if err != nil {
			if opErr, ok := err.(*net.OpError); ok && opErr.Timeout() {
				continue
			}
			logMain.Debug("Failed to accept connection:", err.Error())
		}
		handlers.Add(1)
		go func() {
			host.handler.HandleConnection(Context{conn: conn, host: host})
			handlers.Done()
		}()
	}
}

func (host *Host) GetName() string {
	if host.ServerName != "" {
		return host.ServerName
	} else {
		return host.ListeningAddress()
	}
}

func (host *Host) GetDocumentRoot() (string, error) {
	if host.DocumentRoot != "" {
		return filepath.Abs(host.DocumentRoot)
	}
	return "", errors.New("document_root not set for host:" + host.GetName())
}

func (host *Host) ListeningAddress() string {
	return fmt.Sprintf("%s:%d", host.Address, host.Port)
}

func (host *Host) SetupListener() (ok bool) {
	protocol := "tcp4"

	logMain.Info("Listening to ", host.ListeningAddress())
	tcpAddr, err := net.ResolveTCPAddr(protocol, host.ListeningAddress())

	if err != nil {
		logMain.Error(err.Error())
		return
	}
	host.listener, err = net.ListenTCP(protocol, tcpAddr)
	os.Setenv(SERVER_PORT, strconv.Itoa(int(host.Port)))
	os.Setenv(SERVER_ADDR, host.Address)
	if err != nil {
		logMain.Error(err.Error())
		return false
	}
	return true
}

func parseLocation(host *Host, location map[string]interface{}) (pattern string, handler Handler) {
	locationType, ok := location["type"].(string)
	if !ok {
		return
	}
	pattern, ok = location["pattern"].(string)
	if !ok {
		logMain.Debug("Missing path")
		return
	}

	switch {
	case locationType == "fileserver":
		root, ok := location["root"].(string)
		if !ok {
			root = host.DocumentRoot
		}
		index, ok := location["index"].(string)
		if !ok {
			index = "index.html"
		}
		handler = FileServer(Dir(root), index)
	case locationType == "strip":
		prefix, ok := location["prefix"].(string)
		if !ok {
			logMain.Debug("Missing prefix")
			return
		}
		childMap, ok := location["location"].(map[string]interface{})
		if !ok {
			logMain.Debug("Missing child location")
			return
		}
		_, childLocation := parseLocation(host, childMap)
		handler = StripPrefix(prefix, childLocation)
	case locationType == "redirect":
		loc, ok := location["location"].(string)
		if !ok {
			logMain.Debug("Missing redirect location")
			return
		}
		permanent, ok := location["permanent"].(bool)
		if !ok {
			permanent = false
		}
		handler = RedirectHandler(permanent, loc)
	}

	return pattern, handler
}

func (host *Host) parseLocationsForMultiplexer() {
	for _, location := range host.Locations {
		pattern, handler := parseLocation(host, location)
		if pattern != "" && handler != nil {
			logMain.Info(pattern, " ", handler)
			host.multiplexer.AddHandler(pattern, handler)
		}
	}
}

func (host *Host) setupMultiplexer() {
	host.multiplexer = NewMultiplexer(host)
	host.parseLocationsForMultiplexer()
}

func (host *Host) Init() (ok bool) {
	os.Clearenv()
	var err error

	handler, err := GetProtocolHandler(host.Type)
	if err != nil {
		logMain.Error(err)
		return
	}
	os.Setenv(SERVER_PROTOCOL, host.Type)
	os.Setenv(SERVER_NAME, host.ServerName)
	host.handler = *handler

	host.setupMultiplexer()

	return host.SetupListener()
}
