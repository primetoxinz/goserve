package main

import (
	"bytes"
	"errors"
	"github.com/ryanuber/go-glob"
	"io"
	"os"
	"path/filepath"
	"strings"
)

var metaToHeader = map[string]string{
	"type":     HeaderAccept,
	"language": HeaderAcceptLanguage,
	"charset":  HeaderAcceptCharset,
	"encoding": HeaderAcceptEncoding,
}

func isValidLanguage(lang string) bool {
	return contains(globalConfig.Languages, lang)
}

func GetEncoding(e string) (encoding string, ok bool) {
	if encoding, ok := globalConfig.Encodings[strings.ToLower(e)]; ok {
		return encoding, ok
	}
	return
}

func GetCharset(char string) (charset string, ok bool) {
	if charset, ok := globalConfig.Charsets[char]; ok {
		return charset, ok
	}
	return
}

func (m *MetaTable) findMatchingFiles(fs FileSystem, name string) (err error) {
	m.files = make([]fileMetadata, 0)
	err = fs.Walk(filepath.Dir(name), func(path string, stat os.FileInfo, err error) error {
		relPath := strings.TrimPrefix(path, fs.Root())
		if strings.HasPrefix(relPath, name) {
			//logMain.Info(path, stat, err)
			sizeFunc := func() (int64, error) { return stat.Size(), nil }
			m.files = append(m.files, determineFileMetadata(relPath, sizeFunc))
		}
		return nil
	})

	if err != nil {
		return err
	}

	return nil
}

type MultiMapString map[string][]string

func (m MultiMapString) put(key string, value ...string) {
	if array, ok := m[key]; ok {
		m[key] = append(array, value...)
	} else {
		m[key] = value
	}
}

type MultiMapOffer map[string][]Offer

func (m MultiMapOffer) put(key string, value ...Offer) {
	if array, ok := m[key]; ok {
		m[key] = append(array, value...)
	} else {
		m[key] = value
	}
}

type MetaTable struct {
	types   MultiMapString
	choices MultiMapOffer
	files   []fileMetadata
}

func NewMetaTable() *MetaTable {
	return &MetaTable{make(MultiMapString), make(MultiMapOffer), make([]fileMetadata, 0)}
}

func (m MetaTable) hasMultipleChoices() bool {
	for _, v := range m.choices {
		if len(v) > 1 {
			return true
		}
	}
	return false
}

func (m MetaTable) findFirstChoice() fileMetadata {
	for key, value := range m.choices {
		for _, choice := range value {
			for _, file := range m.files {
				if file[key] == choice.Value {
					return file
				}
			}
		}
	}
	return nil
}

func (m MetaTable) createAlternates() (alternates string) {
	alternates = "{"
	files := make([]string, 0)
	for _, file := range m.files {
		found := false
		for key, value := range file {

			for _, offer := range m.choices[key] {
				if offer.Value == value {
					found = true
				}
			}
		}
		if !found {
			continue
		}

		files = append(files, file.Marshal())
	}
	alternates += strings.Join(files, ",")

	alternates += "}"
	return
}

func (m MetaTable) tabulateFileMeta() {
	for _, meta := range m.files {
		for k, v := range meta {
			m.types.put(k, v)
		}
	}
}

func (m MetaTable) createVaryHeader() string {
	vary := []string{"negotiate"}
	for key, _ := range m.choices {
		vary = append(vary, metaToHeader[key])
	}
	return strings.ToLower(strings.Join(vary, ", "))
}

func (m MetaTable) executeRVAS(request *Request) error {
	//TODO stop using fallback and replace with proper RSVA implementation.
	//This will require basically a full restructure of negotiation as it is currently
	if negotiate, ok := request.Header.Get(HeaderNegotiate); ok {
		if strings.Contains(negotiate, "1.0") {
			//return RSVA1(request, &m)
		}
	}
	err := fallbackRSVA(request, &m)
	if err != nil {
		return err
	}
	return nil
}

func RSVA1(request *Request, metaTable *MetaTable) error {
	//
	//for _, file := range metaTable.files {
	//
	//
	//	for k, v := range file {
	//
	//	}
	//}

	return nil
}

func fallbackRSVA(request *Request, metaTable *MetaTable) error {
	for key, offers := range metaTable.types {
		header := metaToHeader[key]
		if header == "" {
			continue
		}
		choices, err := NegotiateContentSpec(request, offers, header)
		if err != nil {
			return err
		}
		metaTable.choices[key] = choices
	}
	return nil
}

func determineChoices(request *Request, fs FileSystem, name string) (metaTable *MetaTable, err error) {
	metaTable = NewMetaTable()
	err = metaTable.findMatchingFiles(fs, name)
	if err != nil {
		return nil, err
	}

	metaTable.tabulateFileMeta()

	err = metaTable.executeRVAS(request)
	if err != nil {
		return nil, err
	}

	return metaTable, nil
}

func determineContentNegotiation(response *Response, request *Request, fs FileSystem, name string) {
	logMain.Info("Content Negotiation!")

	meta, err := determineChoices(request, fs, name)
	if err != nil {
		if isNotAcceptable(err) {
			response.Header.Set(HeaderTransferEncoding, HeaderTransferEncodingChunked)
			response.SetStatus(StatusNotAcceptable)

			response.Header.Set(HeaderContentType, "text/html")
			if request.Method != MethodHead {
				body := bytes.NewBufferString("<html><ul>")
				for _, files := range meta.files {
					body.WriteString("<li>" + files.Get("basename") + "</li>")
				}
				body.WriteString("</ul></html>")

				var b io.ReadWriter = body
				response.Body = &b
			} else {
				response.Body = nil
			}
		}
		return
	}
	logMain.Info("MetaTable:\n", meta)

	logMain.Info("Choices:", meta.choices)

	response.Header.Set(HeaderVary, meta.createVaryHeader())

	if meta.hasMultipleChoices() {
		updateErrorResponse(response, StatusMultipleChoices)
		alternates := meta.createAlternates()
		logMain.Info(HeaderAlternates, alternates)
		response.Header.Set(HeaderAlternates, alternates)
	} else {
		choice := meta.findFirstChoice()
		if choice != nil {
			name := choice.Get("name")
			serveFile(response, request, fs, name, "")
		}

	}
}

type Offer ContentSpec

func DefaultOffers(offers []string, q float64) []Offer {
	a := make([]Offer, len(offers))
	for i, o := range offers {
		a[i] = Offer{Q: q, Value: o}
	}
	return a
}

func matchesMime(pattern, mime string) bool {
	return glob.Glob(pattern, mime)
}

func matchingSpec(specValue, offer, header string) bool {
	switch header {
	case HeaderAccept:
		return matchesMime(specValue, offer)
	default:
		return specValue == offer
	}
}

func NegotiateContentSpec(request *Request, offers []string, header string) (choices []Offer, err error) {
	choices = make([]Offer, 0)
	specs, err := request.Header.ParseContentSpecHeader(header)
	if err != nil {
		return nil, err
	}
	if specs == nil {
		return DefaultOffers(offers, 1), nil
	}

	bestQ := -1.0

	for _, offer := range offers {
		for _, spec := range specs {

			current := Offer{spec.Q, offer}
			q := spec.Q
			value := spec.Value

			switch {
			case q == 0.0:
				// ignore
			case q < bestQ:
			// better match found
			case matchingSpec(value, offer, header):
				if q > bestQ {
					bestQ = q
					choices = []Offer{current}
				} else if q == bestQ {
					choices = append(choices, current)
				}
			default:
				continue
			}
		}
	}

	if len(choices) == 0 {
		return nil, errors.New("No Choices")
	}
	return choices, nil
}
