package main

import (
	"errors"
	"fmt"
	"io"
	"regexp"
	"strconv"
	"strings"
)

//Header constant values
const (
	HeaderConnectionClose     = "close"
	HeaderConnectionKeepAlive = "keep-alive"

	HeaderTransferEncodingChunked = "chunked"
	HeaderAcceptRangeBytes        = "bytes"
)

//Header Fields
const (
	HeaderDate              = "Date"
	HeaderServer            = "Server"
	HeaderLastModified      = "Last-Modified"
	HeaderContentLength     = "Content-Length"
	HeaderContentType       = "Content-Type"
	HeaderConnection        = "Connection"
	HeaderAllow             = "Allow"
	HeaderIfModifiedSince   = "If-Modified-Since"
	HeaderIfUnmodifiedSince = "If-Unmodified-Since"
	HeaderIfMatch           = "If-Match"
	HeaderIfNoneMatch       = "If-None-Match"
	HeaderETag              = "ETag"
	HeaderLocation          = "Location"
	HeaderReferer           = "Referer"
	HeaderUserAgent         = "User-Agent"

	//Content-Negotiation
	HeaderAccept         = "Accept"
	HeaderAcceptCharset  = "Accept-Charset"
	HeaderAcceptEncoding = "Accept-Encoding"
	HeaderAcceptLanguage = "Accept-Language"
	HeaderNegotiate      = "Negotiate"

	HeaderVary             = "Vary"
	HeaderContentLanguage  = "Content-Language"
	HeaderContentLocation  = "Content-Location"
	HeaderContentEncoding  = "Content-Encoding"
	HeaderTransferEncoding = "Transfer-Encoding"
	HeaderAlternates       = "Alternates"
	HeaderTCN              = "TCN"

	//Ranges
	HeaderRange        = "Range"
	HeaderIfRange      = "If-Range"
	HeaderAcceptRange  = "Accept-Range"
	HeaderContentRange = "Content-Range"

	//Authorization
	HeaderAuthorization      = "Authorization"
	HeaderWWWAuthenticate    = "WWW-Authenticate"
	HeaderAuthenticationInfo = "Authentication-Info"
)

type Header map[string][]string

func (h Header) Write(writer io.Writer) error {
	for k, v := range h {
		if len(v) == 0 {
			continue
		}
		_, err := fmt.Fprintf(writer, "%s: %s\r\n", k, strings.Join(v, " "))
		if err != nil {
			return err
		}
	}
	return nil
}

func (h Header) Add(fieldName string, fieldValues ...string) {
	if val, ok := h[fieldName]; ok {
		fieldValues = append(val, fieldValues...)
	}
	h.Set(fieldName, fieldValues...)
}

func (h Header) Has(fieldName string) bool {
	_, ok := h[fieldName]
	return ok
}

func (h Header) SetInt(fieldName string, number int) {
	h.Set(fieldName, strconv.Itoa(number))
}

func (h Header) Set(fieldName string, fieldValues ...string) {
	h[fieldName] = fieldValues
}

func (h Header) Get(fieldName string) (value string, ok bool) {
	if v := h[fieldName]; len(v) > 0 {
		return v[0], true
	}
	return "", false
}

func (h Header) Delete(fieldName string) {
	h[fieldName] = nil
}

func (h Header) ForwardField(fieldValue string, requestHeader Header) {
	if requestHeader != nil {
		value, ok := requestHeader[fieldValue]
		if ok {
			h.Set(fieldValue, value...)
		}
	}
}

type ContentSpec struct {
	Q     float64
	Value string
}

const specPattern string = `(.*);\sq=(\d\.\d+)`

func parseContentSpec(v string) (ContentSpec, error) {
	exp := regexp.MustCompile(specPattern)
	if exp.MatchString(v) {
		capture := exp.FindStringSubmatch(v)
		Q, err := strconv.ParseFloat(capture[2], 64)
		if err != nil {
			logMain.Error(err)
		}
		value := strings.TrimSpace(capture[1])
		return ContentSpec{Q: Q, Value: value}, nil
	}
	return ContentSpec{}, errors.New("Not able to parse content spec from: " + v)
}

func validateSpecValues(spec ContentSpec, header string) bool {
	if spec.Q == 0 {
		//Specs with no q value should be ignored.
		return false
	}
	switch header {
	case HeaderAccept:
		return isValidMimeType(spec.Value, true)
	case HeaderAcceptLanguage:
		return isValidLanguage(spec.Value)
	case HeaderAcceptCharset:
		_, ok := GetCharset(spec.Value)
		return ok
	case HeaderAcceptEncoding:
		_, ok := GetEncoding(spec.Value)
		return ok
	}
	return false
}

func isNotAcceptable(err error) bool {
	_, ok := err.(invalidContentSpec)
	return ok
}
func (h Header) ParseContentSpecHeader(header string) ([]ContentSpec, error) {
	a, ok := h.Get(header)
	if ok {
		values := strings.Split(a, ",")
		specs := make([]ContentSpec, 0)
		hasValid := false
		for _, v := range values {
			spec, err := parseContentSpec(v)

			if err != nil {
				logMain.Error(err)
				continue
			}
			if validateSpecValues(spec, header) {
				specs = append(specs, spec)
				hasValid = true
			}
		}
		if !hasValid {
			return nil, invalidContentSpec("Found no valid content specifications:" + strings.Join(values, ","))
		}

		return specs, nil
	}
	return nil, nil
}
