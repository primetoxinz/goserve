package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"mime"
	"mime/multipart"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

//noinspection GoSnakeCaseUsage
const (
	XFORMWWWURLENCODED = "application/x-form-www-urlencoded"
	MULTIPARTFORMDATA  = "multipart/form-data"
)
const (
	QUERY_STRING    = "QUERY_STRING"
	HTTP_USER_AGENT = "HTTP_USER_AGENT"
	HTTP_REFERER    = "HTTP_REFERER"
	REQUEST_METHOD  = "REQUEST_METHOD"
	REMOTE_USER     = "REMOTE_USER"
	SCRIPT_NAME     = "SCRIPT_NAME"
	SCRIPT_FILENAME = "SCRIPT_FILENAME"
	SCRIPT_URI      = "SCRIPT_URI"
	SERVER_SOFTWARE = "SERVER_SOFTWARE"
	SERVER_PORT     = "SERVER_PORT"
	AUTH_TYPE       = "AUTH_TYPE"
	SERVER_ADDR     = "SERVER_ADDR"
	SERVER_PROTOCOL = "SERVER_PROTOCOL"
	SERVER_NAME     = "SERVER_NAME"
	REMOTE_ADDR     = "REMOTE_ADDR"
)

const (
	defaultMaxMemory = 32 << 20 // 32 MB
)

var (
	MULTIPART_BOUNDARY = regexp.MustCompile("^multipart/form-data; boundary=(.*)$")
)

func setupEnviron(request *Request) {
	os.Setenv(SERVER_SOFTWARE, globalConfig.ServerInfo)
	os.Setenv(QUERY_STRING, request.URL.Query().Encode())

	if agent, ok := request.Header.Get(HeaderUserAgent); ok {
		os.Setenv(HTTP_USER_AGENT, agent)
	}
	if r, ok := request.Header.Get(HeaderReferer); ok {
		os.Setenv(HTTP_REFERER, r)
	}
	os.Setenv(REMOTE_USER, request.User)
	os.Setenv(REQUEST_METHOD, request.Method)
	logMain.Info(os.Environ())
}

func requestBodyToString(r *Request) (string, error) {
	var body = *r.Body
	b := make([]byte, r.ContentLength)
	_, err := body.Read(b)
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(string(b)), nil
}

func parseMultipartBoundary(contentType string) string {
	if MULTIPART_BOUNDARY.MatchString(contentType) {
		matches := MULTIPART_BOUNDARY.FindStringSubmatch(contentType)
		return matches[1]
	}
	return ""
}

func parseMultipart(response *Response, request *Request, v string) (form *multipart.Form, err error) {

	d, params, err := mime.ParseMediaType(v)

	if err != nil || !(d == "multipart/form-data") {
		return nil, err
	}

	boundary, ok := params["boundary"]
	if !ok {
		return nil, errors.New("missing boundary")
	}
	mp := multipart.NewReader(*request.Body, boundary)
	form, err = mp.ReadForm(defaultMaxMemory)
	if err != nil {
		return nil, err
	}

	return form, nil
}

func serveCGI(response *Response, request *Request, fs FileSystem, name string, stat os.FileInfo) (err error) {
	var body = ""
	if request.Method == MethodPost {
		if ct, ok := request.Header.Get(HeaderContentType); ok {
			var query string
			switch {
			case ct == XFORMWWWURLENCODED:
				query, err = requestBodyToString(request)
				if err != nil {
					return err
				}
				request.URL.RawQuery = query
			case strings.HasPrefix(ct, MULTIPARTFORMDATA):

				body, err = requestBodyToString(request)
				if err != nil {
					return err
				}
				//form, _ := parseMultipart(response, request, ct)
				//request.MultipartForm = form
			}

		}
	}

	relpath := fs.GetFullPath(name)

	file, _ := filepath.Abs(relpath)
	cmd := exec.Command(file)
	cmd.Dir = filepath.Dir(file)

	os.Setenv(SCRIPT_URI, request.RequestURI)
	os.Setenv(SCRIPT_FILENAME, file)
	os.Setenv(SCRIPT_NAME, relpath)

	setupEnviron(request)

	stdin, err := cmd.StdinPipe()
	if err != nil {
		fmt.Println(err)
	}
	_, _ = io.WriteString(stdin, body)
	stdin.Close()
	b := bytes.NewBufferString("")
	cmd.Stdout = b

	if err = cmd.Start(); err != nil {
		return err
	}
	if err = cmd.Wait(); err != nil {
		return err
	}

	logMain.Info(cmd.ProcessState.ExitCode())
	response.SetStatus(StatusOK)
	contents, n, err := parseCGIHeaders(response, b.String())
	if err != nil {
		return err
	}

	if n == 0 {
		updateErrorResponse(response, StatusInternalServerError)
		return nil
	}

	response.Header.Set(HeaderTransferEncoding, HeaderTransferEncodingChunked)

	c := strings.Join(contents, "\n")
	if c != "" {
		buff := bytes.NewBufferString(c)
		response.ContentLength = int64(buff.Len())
		var body io.ReadWriter = buff
		response.Body = &body
	}

	return err
}

type cgiHandler func(vars []string, r *Response) (cont bool, err error)

type CGIHeader interface {
	Apply(vars []string, r *Response) (bool, error)
	Match(line string) (bool, []string)
}

type basicCGIHeader struct {
	pattern *regexp.Regexp
	apply   func(vars []string, r *Response) (cont bool, err error)
}

func (b *basicCGIHeader) Apply(vars []string, r *Response) (cont bool, err error) {
	return b.apply(vars, r)
}

func (b *basicCGIHeader) Match(line string) (bool, []string) {
	if b.pattern.MatchString(line) {
		return true, b.pattern.FindStringSubmatch(line)
	}
	return false, nil
}

func NewCGIHeader(pattern string, apply cgiHandler) CGIHeader {
	return &basicCGIHeader{pattern: regexp.MustCompile(pattern), apply: apply}
}

var (
	CGIHeaders = map[string]CGIHeader{
		"location": NewCGIHeader(`Location:(.*)`, func(vars []string, r *Response) (cont bool, err error) {
			u, err := url.Parse(strings.TrimSpace(vars[1]))
			if err != nil {
				return false, err
			}
			r.Header.Set(HeaderLocation, u.String())
			updateErrorResponse(r, StatusFound)
			return false, nil
		}),
		"status": NewCGIHeader(`^Status: (\d{3}) (.*)$`, func(vars []string, r *Response) (cont bool, err error) {
			if statusCode, err := strconv.Atoi(vars[1]); err == nil {
				r.StatusCode = statusCode
				r.Status = vars[2]
				return true, nil
			}
			return false, err
		}),
		"content-type": NewCGIHeader(`^Content-type:(.*)`, func(vars []string, r *Response) (cont bool, err error) {
			c := strings.TrimSpace(vars[1])
			if isValidMimeType(c, false) {
				r.Header.Set(HeaderContentType, c)
				return true, nil
			}
			return false, errors.New("invalid content type")
		}),
	}
)

func parseCGIHeaders(response *Response, output string) (contents []string, n int, err error) {
	lines := strings.Split(output, "\n")
	matched := make(map[int]bool, 0)
	contents = make([]string, 0)
	for _, header := range CGIHeaders {
		for i, line := range lines {
			if ok, vars := header.Match(line); !matched[i] && ok {
				n++
				matched[i] = true
				cont, e2 := header.Apply(vars, response)
				err = e2
				if !cont {
					return
				}
				if e2 != nil {

					return
				}
			}
		}
	}

	for i, line := range lines {
		if !matched[i] {
			contents = append(contents, line)
		}
	}

	return contents, n, err
}
