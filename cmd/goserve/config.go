package main

import (
	"github.com/ryanuber/go-glob"
	"github.com/spf13/viper"
	"path/filepath"
	"time"
)

type MimeType struct {
	Type       string   `mapstructure:"type"`
	Extensions []string `mapstructure:"exts"`
}

type DigestSettings struct {
	Nonce  string `mapstructure:"nonce_key"`
	Opaque string `mapstructure:"opaque_key"`
}

type Limits struct {
	URI    int `mapstructure:"uri"`
	Entity int `mapstructure:"entity"`
}

type GlobalConfig struct {
	*HostList
	SupportedMethods             []string          `mapstructure:"supported_methods"`
	PermittedMethods             []string          `mapstructure:"permitted_methods"`
	HTTPVersion                  string            `mapstructure:"http_version"`
	ReadTimeout                  time.Duration     `mapstructure:"read_timeout"`
	WriteTimeout                 time.Duration     `mapstructure:"write_timeout"`
	IdleTimeout                  time.Duration     `mapstructure:"idle_timeout"`
	ServerInfo                   string            `mapstructure:"server_info"`
	MimeTypes                    []MimeType        `mapstructure:"mime_types"`
	DefaultConnectionPersistence string            `mapstructure:"default_connection_persistence"`
	DirectoryTemplatePath        string            `mapstructure:"directory_template"`
	Charsets                     map[string]string `mapstructure:"charsets"`
	Languages                    []string          `mapstructure:"languages"`
	Encodings                    map[string]string `mapstructure:"encodings"`
	DigestSettings               DigestSettings    `mapstructure:"digest"`
	Limits                       Limits            `mapstructure:"limits"`
}

func configReadError(err error) bool {
	if _, ok := err.(viper.ConfigFileNotFoundError); ok {
		logMain.Fatal(err.Error())
		return true
	} else {
		logMain.Fatal(err.Error())
		return true
	}
}

func ReadHosts(viper *viper.Viper) *HostList {
	hostList := new(HostList)
	err := viper.Unmarshal(hostList)
	if err != nil {
		logMain.Error(err.Error())
	}

	return hostList
}

func ReadIncludeConfig(file string) *HostList {
	config := viper.New()
	config.SetConfigType("toml")
	config.SetConfigFile(file)
	_ = config.ReadInConfig()

	hostList := ReadHosts(config)

	return hostList
}

func NewGlobalConfig() *GlobalConfig {
	config := viper.New()
	config.SetConfigType("toml")
	config.SetConfigName("goserve")
	config.AddConfigPath("/etc/webserver/")
	config.AddConfigPath("./configs/")

	initMimeCache()

	err := config.ReadInConfig()
	handleError(err, configReadError)
	includes := config.GetStringSlice("includes")
	var hosts []*Host
	hosts = make([]*Host, 0)
	for _, include := range includes {
		matches, err := filepath.Glob(include)
		if err != nil {
			logMain.Error(err.Error())
		}
		for _, match := range matches {
			tempHostList := ReadIncludeConfig(match)
			hosts = append(hosts, tempHostList.Hosts...)
		}
	}
	hostList := &HostList{Hosts: hosts}

	tmp := hostList.Hosts[:0]
	for _, host := range hostList.Hosts {
		if host.Init() {
			//Only keep hosts that could successfully init
			tmp = append(tmp, host)
		}
	}

	global := new(GlobalConfig)
	err = config.Unmarshal(global)
	if err != nil {
		logMain.Error(err)
	}
	global.HostList = hostList

	global.DirectoryTemplatePath = filepath.Dir(config.ConfigFileUsed()) + "/" + global.DirectoryTemplatePath

	logMain.Info("Starting ", global.ServerInfo)
	logMain.Debug(global.MimeTypes)
	logMain.Debug(global.Charsets)
	logMain.Debug(global.Languages)
	logMain.Debug(global.Encodings)
	return global
}

var mimeCache map[string]string

func initMimeCache() {
	mimeCache = make(map[string]string)
}

func findMimeType(name string) string {
	for _, entry := range globalConfig.MimeTypes {
		for _, ext := range entry.Extensions {
			if ext == name {
				return entry.Type
			}
		}
	}
	return ApplicationOctetStream
}

func isValidMimeType(mime string, shouldGlob bool) bool {
	for _, entry := range globalConfig.MimeTypes {
		if (shouldGlob && glob.Glob(mime, entry.Type)) || entry.Type == mime {
			return true
		}
	}
	return false
}

func GetMimeType(name string) string {
	logMain.Debug("Finding Mime for: ", name)
	mime, ok := mimeCache[name]
	if !ok {
		m := findMimeType(name)
		if m != "" {
			mime = m
			mimeCache[name] = m
		}
	}
	return mime
}
