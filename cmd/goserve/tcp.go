package main

type TCPEcho struct{}

func (t *TCPEcho) HandleConnection(ctx Context) {
	conn := ctx.conn
	defer conn.Close()
	for {
		buf := make([]byte, 1024)
		size, err := conn.Read(buf)
		if err != nil {
			return
		}
		data := buf[:size]
		conn.Write(data)
	}
}
