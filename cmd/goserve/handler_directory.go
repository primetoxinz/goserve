package main

import (
	"fmt"
	"github.com/Masterminds/sprig"
	"html/template"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"
)

func newDirectoryData(name, path string) directoryData {
	list, err := ioutil.ReadDir(path)
	if err != nil {
		logMain.Error(err)
	}

	files := make([]fileData, 0)
	files = append(files, fileData{"..", "..", -1, "-", "", true})

	for _, f := range list {
		uri := "./" + f.Name()
		d := fileData{f.Name(), uri, f.ModTime().Unix(), f.ModTime().Format(time.RFC1123), fmt.Sprintf("%d", f.Size()), f.IsDir()}
		files = append(files, d)
	}

	return directoryData{name, files}
}

func directoryListing(response *Response, request *Request, name, path string, stat os.FileInfo) {
	tpl := template.New(filepath.Base(globalConfig.DirectoryTemplatePath)).Funcs(sprig.FuncMap())
	t := template.Must(tpl.ParseFiles(globalConfig.DirectoryTemplatePath))
	var buf closableBuffer
	err := t.Execute(&buf, newDirectoryData(name, path))
	if err != nil {
		logMain.Error(err)
	}

	logMain.Debug(buf.Len())
	sizeFunc := func() (int64, error) { return int64(buf.Len()), nil }

	serveContent(response, request, "directory.html", time.Now(), sizeFunc, &buf)
}

type directoryData struct {
	Path  string
	Files []fileData
}

type fileData struct {
	Name       string
	RequestURI string
	ModEpoch   int64
	ModTime    string
	Size       string
	IsDir      bool
}
