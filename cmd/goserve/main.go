package main

import (
	"github.com/hhkbp2/go-logging"
	"sync"
)

var globalConfig *GlobalConfig

func main() {
	defer logging.Shutdown()
	InitLogger()
	InitProtocolHandler()
	globalConfig = NewGlobalConfig()

	for _, host := range globalConfig.HostList.Hosts {
		go host.Listen()
	}

	var wg sync.WaitGroup
	wg.Add(1)
	wg.Wait()

}
