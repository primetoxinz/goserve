package main

import (
	"bufio"
	"bytes"
	"io"
	"net/url"
	"os"
	"regexp"
	"sort"
	"strings"
)

type handlerContext struct {
	patten string
	regex  *regexp.Regexp
}

type Handler interface {
	Handle(ctx *handlerContext, response *Response, request *Request)
}

type multiplexEntry struct {
	handler Handler
	pattern string
}

type Multiplexer struct {
	locations map[string]multiplexEntry
	entries   []multiplexEntry
	host      *Host
}

func NewMultiplexer(host *Host) Multiplexer {
	return Multiplexer{make(map[string]multiplexEntry), make([]multiplexEntry, 0), host}
}

func (m *Multiplexer) addHandler(path string, handler Handler) {
	entry := multiplexEntry{handler: handler, pattern: path}
	m.locations[path] = entry
	m.entries = append(m.entries, entry)
	sort.Slice(m.entries, func(a, b int) bool {
		return m.entries[a].pattern > m.entries[b].pattern
	})

}

func (m *Multiplexer) AddHandler(path string, handler Handler) {
	m.addHandler(path, handler)
}

type multiplexMatch struct {
	handler Handler
	ctx     *handlerContext
}

func (m *Multiplexer) match(path string) (match multiplexMatch, ok bool) {

	v, found := m.locations[path]
	if found {
		ctx := handlerContext{v.pattern, nil}
		return multiplexMatch{v.handler, &ctx}, true
	}

	for _, entry := range m.entries {
		exp, err := regexp.Compile(entry.pattern)
		if err != nil {
			logMain.Error(err)
		}

		if exp.MatchString(path) {
			ctx := handlerContext{entry.pattern, exp}
			return multiplexMatch{entry.handler, &ctx}, true
		}
	}
	return
}

func isMethodAllowed(a *AccessFile, err error, request *Request) bool {
	return contains(getAllowedMethods(a, err), request.Method)
}

func getAllowedMethods(a *AccessFile, err error) []string {
	if err != nil {
		return globalConfig.PermittedMethods
	}
	return a.GetAllowedMethods()
}

func (m *Multiplexer) Handle(response *Response, request *Request) {
	header := response.Header
	header.ForwardField(HeaderConnection, request.Header)

	access, err := m.host.AuthHandler.FindAccessFile(response, request)

	if !os.IsNotExist(err) {
		if !m.host.AuthHandler.IsAuthorized(access, err, response, request) {
			return
		}
	}

	switch {
	case request.Method == MethodOptions:
		response.SetStatus(StatusOK)
		var methods []string
		if request.RequestURI == "*" {
			methods = globalConfig.SupportedMethods
		} else if err == nil {
			methods = access.GetAllowedMethods()
		} else {
			methods = globalConfig.PermittedMethods
		}
		response.Header.Set(HeaderAllow, methods...)
		return
	case request.Method == MethodTrace:
		response.SetStatus(StatusOK)
		response.Header.Set(HeaderContentType, MessageHTTP)
		var buffer bytes.Buffer
		writer := bufio.NewWriter(&buffer)
		request.WriteRequest(writer)
		var body io.ReadWriter = &buffer
		response.Body = &body
		response.ContentLength = int64(buffer.Len())
		break
	case !isMethodAllowed(access, err, request):
		updateErrorResponse(response, StatusMethodNotAllowed)
		response.Header.Set(HeaderAllow, getAllowedMethods(access, err)...)
		break
	default:
		result, ok := m.match(request.URL.Path)
		if ok {
			logMain.Debug("Matching Entry:", result.ctx.patten)
			result.handler.Handle(result.ctx, response, request)
		}
	}

}

type handlerFunc struct {
	function func(ctx *handlerContext, response *Response, request *Request)
}

func (h handlerFunc) Handle(ctx *handlerContext, response *Response, request *Request) {
	h.function(ctx, response, request)
}

func HandlerFunc(function func(ctx *handlerContext, response *Response, request *Request)) Handler {
	return handlerFunc{function}
}

func StripPrefix(prefix string, h Handler) Handler {
	if prefix == "" {
		return h
	}
	return HandlerFunc(func(ctx *handlerContext, w *Response, r *Request) {
		if p := strings.TrimPrefix(r.URL.Path, prefix); len(p) < len(r.URL.Path) {
			r2 := new(Request)
			*r2 = *r
			r2.URL = new(url.URL)
			*r2.URL = *r.URL
			r2.URL.Path = p
			h.Handle(ctx, w, r2)
		} else {
			updateErrorResponse(w, StatusNotFound)
		}
		return
	})

}

func RedirectHandler(permanent bool, location string) Handler {
	return HandlerFunc(func(ctx *handlerContext, w *Response, r *Request) {
		newLocation := ctx.regex.ReplaceAllString(r.URL.Path, location)
		logMain.Debug("Redirecting to ", newLocation)
		w.Header.Set("Location", newLocation)
		if permanent {
			w.SetStatus(StatusMovedPermanently)
		} else {
			w.SetStatus(StatusFound)
		}
	})
}
