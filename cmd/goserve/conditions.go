package main

import (
	"encoding/hex"
	"fmt"
	"os"
	"strings"
	"syscall"
	"time"
)

type condResult int

const (
	condNone condResult = iota
	condTrue
	condFalse
)

func strongETag(stat os.FileInfo) string {
	sysstat, ok := stat.Sys().(*syscall.Stat_t)

	if !ok {
		logMain.Error("Failed to cast syscall Stat_t")
	}
	inode := sysstat.Ino
	mtime := sysstat.Mtim.Nsec * 1000
	size := sysstat.Size

	//Computes ETag as inode-sizeinbytes-mtimeinmicroseconds-filenamehex
	return fmt.Sprintf("\"%x-%x-%x-%s\"", inode, size, mtime, hex.EncodeToString([]byte(stat.Name())))
}

func parseEtags(header string) (etags []string) {
	if header == "" {
		return
	}
	etags = strings.Split(header, ",")
	for i, e := range etags {
		etags[i] = strings.TrimSpace(e)
	}
	return etags
}

func matchEtag(remote, local string) bool {
	if remote == "*" {
		return true
	}
	return remote == local
}

//Returns true if precondition failed
func checkPreconditions(response *Response, request *Request, modtime time.Time) (done bool) {

	result := conditionIfRange(response, request)
	if result != condNone {
		return false
	}

	result = conditionIfMatch(response, request)
	if result == condNone {
		result = conditionIfUnmodifiedSince(response, request, modtime)
	}

	if result == condFalse {
		updateErrorResponse(response, StatusPreconditionFailed)
		return true
	}

	result = conditionIfNoneMatch(response, request)
	if result == condFalse {
		if request.Method == MethodGet || request.Method == MethodHead {
			//304 MUST not have a message-body
			response.SetStatus(StatusNotModified)
			return true
		} else {
			updateErrorResponse(response, StatusPreconditionFailed)
			return true
		}
	} else if result == condNone {
		if conditionIfModifiedSince(response, request, modtime) == condFalse {
			//304 MUST not have a message-body
			response.SetStatus(StatusNotModified)
			return true
		}
	}

	return false
}

func conditionIfMatch(response *Response, request *Request) condResult {
	ifMatch, ok := request.Header.Get(HeaderIfMatch)
	if !ok {
		return condNone
	}

	requestETags := parseEtags(ifMatch)
	if requestETags == nil {
		return condNone
	}
	etag, _ := response.Header.Get(HeaderETag)
	for _, h := range requestETags {
		if matchEtag(h, etag) {
			return condTrue
		}
	}
	return condFalse
}

func conditionIfModifiedSince(response *Response, request *Request, modtime time.Time) condResult {
	header, ok := request.Header.Get(HeaderIfModifiedSince)
	if !ok {
		return condNone
	}
	since := ParseTime(header)
	if modtime.After(since) {
		return condTrue
	}
	return condFalse
}

func conditionIfUnmodifiedSince(response *Response, request *Request, modtime time.Time) condResult {
	header, ok := request.Header.Get(HeaderIfUnmodifiedSince)
	if !ok {
		return condNone
	}
	since := ParseTime(header)
	if modtime.After(since) {
		return condFalse
	}
	return condTrue
}

func conditionIfNoneMatch(response *Response, request *Request) condResult {
	ifMatch, ok := request.Header.Get(HeaderIfNoneMatch)
	if !ok {
		return condNone
	}

	requestETags := parseEtags(ifMatch)
	if requestETags == nil {
		return condNone
	}
	etag, _ := response.Header.Get(HeaderETag)
	for _, h := range requestETags {
		if matchEtag(h, etag) {
			return condFalse
		}
	}
	return condTrue

}

func conditionIfRange(response *Response, request *Request) condResult {
	rang, hasRange := request.Header.Get(HeaderRange)
	ifRange, hasIf := request.Header.Get(HeaderIfRange)

	if hasRange && hasIf {

		etag, ok := response.Header.Get(HeaderETag)
		if ok {
			if ifRange == etag {
				return condTrue
			} else {
				///rfc2616#section-14.27
				//If the entity tag does not match, then the server SHOULD
				//   return the entire entity using a 200 (OK) response.
				request.Header.Delete(HeaderRange)
			}
		}

		logMain.Info(rang, ifRange)

	}

	return condNone
}
