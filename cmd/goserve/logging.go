package main

import (
	"fmt"
	"github.com/hhkbp2/go-logging"
	"os"
	"path/filepath"
)

var directories = []string{"/etc/webserver/", "./configs/logs/"}

var logMain logging.Logger
var logAccess logging.Logger

func InitLogger() {
	var configFile string
	for _, dir := range directories {
		file, _ := filepath.Abs(fmt.Sprintf("%s/loggers.json", dir))
		if _, err := os.Stat(file); os.IsNotExist(err) {
			fmt.Printf("%s does not exist\n", file)
			continue
		}

		configFile = file

		if configFile != "" {
			break
		}
	}

	if err := logging.ApplyConfigFile(configFile); err != nil {
		panic(err.Error())
	}
	logMain = logging.GetLogger("main")
	logAccess = logging.GetLogger("access")
}
