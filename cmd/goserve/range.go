package main

import (
	"fmt"
	"io"
	"math"
	"strconv"
	"strings"
)

type Range interface {
	Read(content io.ReadSeeker) ([]byte, error)
	Get() (start, end int64)
	Header() string
}

type byteRange struct {
	start, end int64
}

type RangeSet []Range

func (s RangeSet) SetContentRangeHeader(response *Response) {
	header := ""
	for _, r := range s {
		header += fmt.Sprintf("%s/%d", r.Header(), response.ContentLength)
	}
	response.Header.Set(HeaderContentRange, header)
}

func (s RangeSet) Read(reader io.ReadSeeker, length int64) ([]byte, error) {
	buf := make([]byte, 0)

	satisfiable := false
	for _, r := range s {
		start, _ := r.Get()
		if start < length {
			satisfiable = true
		}
	}
	if !satisfiable {
		return nil, unsatisfiableRangeSet("Range set unsatisfiable")
	}

	for _, r := range s {
		v, err := r.Read(reader)
		if err != nil {
			return nil, err
		}
		buf = append(buf, v...)
	}
	return buf, nil
}

func (b *byteRange) Get() (start, end int64) {
	return b.start, b.end
}
func (b *byteRange) Len() int64 {
	diff := float64(b.end - b.start)
	if diff < 0 {
		return int64(math.Abs(diff))
	}
	return int64(diff + 1)
}

func (b *byteRange) Read(content io.ReadSeeker) ([]byte, error) {
	buf := make([]byte, b.Len())
	_, err := content.Seek(b.start, io.SeekStart)
	if err != nil {
		return nil, err
	}
	_, err = content.Read(buf)
	if err != nil {
		return nil, err
	}
	return buf, nil
}

func (b *byteRange) Header() string {
	return fmt.Sprintf("%s %d-%d", "bytes", b.start, b.end)
}

func parseByteRangeSet(rangeSet []string) (ranges RangeSet, err error) {
	ranges = make(RangeSet, 0)
	for _, r := range rangeSet {
		r1, err := parseByteRange(r)
		if err != nil {
			return nil, err
		}
		ranges = append(ranges, r1)
	}
	return
}

func parseByteRange(r string) (br *byteRange, err error) {
	logMain.Info("bytes=", r)
	r = strings.TrimSpace(r)
	i := strings.Index(r, "-")
	if i < 0 {
		return nil, badRangeError("Missing -")
	}
	numbers := strings.Split(r, "-")
	logMain.Info(numbers)

	if len(numbers) != 2 {
		return nil, badRangeError("Too many - to split on")
	}

	minStr, maxStr := numbers[0], numbers[1]

	var min int64 = 0
	if minStr != "" {
		min, err = strconv.ParseInt(minStr, 10, 64)
		if err != nil {
			return nil, err
		}
	}

	var max int64 = -1
	if maxStr != "" {

		//Add back the dash for max if no min, as it should be negative
		if minStr == "" {
			maxStr = "-" + maxStr
		}

		max, err = strconv.ParseInt(maxStr, 10, 64)
		if err != nil {
			return nil, err
		}
	}

	return &byteRange{min, max}, nil
}

func splitRangeSpec(r string) (unit string, rangeSet []string, err error) {
	split := strings.Split(strings.TrimSpace(r), "=")
	if len(split) < 2 {
		return "", nil, badRangeError("Bad Range")
	}
	unit = split[0]
	setString := split[1]
	if !strings.ContainsRune(setString, ',') {
		rangeSet = []string{setString}
	} else {
		rangeSet = strings.Split(setString, ",")
	}
	return
}

func ParseRange(r string) (RangeSet, error) {
	unit, rangeSet, err := splitRangeSpec(r)
	if err != nil {
		return nil, err
	}
	logMain.Info(r)
	switch unit {
	case "bytes":
		return parseByteRangeSet(rangeSet)
	}
	return nil, badRangeError("Bad Range")
}
