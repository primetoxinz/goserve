package main

import (
	"errors"
	"net"
)

type Context struct {
	conn net.Conn
	host *Host
}

type ProtocolHandler interface {
	HandleConnection(ctx Context)
}

type ProtocolHandlerRegistry struct {
	registry map[string]*ProtocolHandler
}

var instance *ProtocolHandlerRegistry = nil

func get() *ProtocolHandlerRegistry {
	if instance == nil {
		instance = &ProtocolHandlerRegistry{}
		instance.registry = make(map[string]*ProtocolHandler)
	}
	return instance
}

func RegisterHandler(key string, handler *ProtocolHandler) {
	get().registry[key] = handler
	logMain.Info("Registered ProtocolHandler:", key)
}

func InitProtocolHandler() {
	var tcp ProtocolHandler = &TCPEcho{}
	RegisterHandler("tcpecho", &tcp)
	var http ProtocolHandler = &HTTPServer{}
	RegisterHandler("http", &http)
}

func GetProtocolHandler(key string) (*ProtocolHandler, error) {
	registry := get().registry
	if handler, ok := registry[key]; ok {
		return handler, nil
	}

	return nil, errors.New("protocol handler " + key + " not found.")
}
