package main

import (
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"io"
	"strings"
	"time"
)

func contains(slice []string, a string) bool {
	for _, n := range slice {
		if n == a {
			return true
		}
	}
	return false
}

func Pairs(string, sep string) (string, string, error) {
	array := strings.Split(string, sep)
	if len(array) > 2 {
		return "", "", errors.New("Too many entries in pair")
	}
	return array[0], array[1], nil
}

func FormatTime(t time.Time) string {
	GMT, _ := time.LoadLocation("GMT")
	return t.In(GMT).Format(time.RFC1123)
}

func ParseTime(s string) time.Time {
	GMT, _ := time.LoadLocation("GMT")
	t, err := time.Parse(time.RFC1123, s)
	if err != nil {
		logMain.Error(err)
	}
	return t.In(GMT)
}

func CRLF(writer io.Writer) error {
	_, err := writer.Write([]byte("\r\n"))
	return err
}

func MD5(text string) string {
	algorithm := md5.New()
	algorithm.Write([]byte(text))
	return hex.EncodeToString(algorithm.Sum(nil))
}

func Base64(text string) string {
	return base64.StdEncoding.EncodeToString([]byte(text))
}
