package main

import (
	"bufio"
	"errors"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

type AuthScheme string

const (
	Basic  AuthScheme = "Basic"
	Digest AuthScheme = "Digest"
)

type AccessFile struct {
	Type        AuthScheme
	Realm       string
	Credentials Credentials
	Methods     []string
}

var (
	PatternType       = regexp.MustCompile("^authorization-type=(.*)$")
	PatternRealm      = regexp.MustCompile("^realm=\"(.*)\"")
	PatternCredential = regexp.MustCompile("^(\\w+):?(?P<realm>.*)?:(.*)$")
	PatternMethod     = regexp.MustCompile("^ALLOW-(.*)$")
)

var (
	NoAuthorizationFileFound = errors.New("no authorization file found")
)

func ReadFileLines(path string) (lines []string, err error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)
	lines = make([]string, 0)
	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
	}

	if err = scanner.Err(); err != nil {
		return nil, err
	}

	return lines, nil
}

func NewAuthorizationFile(path string) (a *AccessFile, err error) {
	lines, err := ReadFileLines(path)
	if err != nil {
		return nil, err
	}

	var authScheme AuthScheme
	var realm string
	var cred = make(Credentials)
	var methods []string
	for _, line := range lines {

		switch {
		case PatternMethod.MatchString(line):
			sub := PatternMethod.FindStringSubmatch(line)
			method, err := validateAllowMethod(sub[1])
			if err != nil {
				return nil, err
			}
			methods = append(methods, method)
			break
		case PatternType.MatchString(line):
			sub := PatternType.FindStringSubmatch(line)
			authScheme, err = validateAuthScheme(sub[1])
			if err != nil {
				return nil, err
			}
			break

		case PatternCredential.MatchString(line):

			names := PatternCredential.SubexpNames()
			logMain.Info(names)
			sub := PatternCredential.FindStringSubmatch(line)

			if len(sub) == 4 {
				if sub[2] != "" {
					cred[sub[1]] = map[string]string{
						"realm":    sub[2],
						"password": sub[3],
					}
				} else {
					cred[sub[1]] = sub[3]
				}

			} else {
				return nil, errors.New("invalid credential")
			}

			break

		case PatternRealm.MatchString(line):
			sub := PatternRealm.FindStringSubmatch(line)

			if len(sub) == 2 {
				realm = sub[1]
			} else {
				return nil, errors.New("invalid realm")
			}
			break
		}
	}

	return &AccessFile{authScheme, realm, cred, methods}, nil
}

func (a *AccessHandler) validAuthorizationFileName(name string) bool {
	return name == a.AuthorizationFileName
}

func (a *AccessHandler) findAccessFile(relPath string, fs FileSystem) (*AccessFile, error) {

	path := relPath
	d, err := fs.IsDir(relPath)
	if !os.IsNotExist(err) && err != nil {
		return nil, err
	}

	if os.IsNotExist(err) || !d {
		path = filepath.Dir(relPath)
	}
	logMain.Info(path)
	files, err := fs.List(path)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		if !file.IsDir() {
			if a.validAuthorizationFileName(file.Name()) {
				auth, err := NewAuthorizationFile(fs.GetFullPath(path) + "/" + file.Name())
				if err != nil {
					return nil, err
				}
				return auth, nil
			}
		}
	}

	if path == "/" {
		return nil, NoAuthorizationFileFound
	}
	logMain.Info(path)
	return a.findAccessFile(filepath.Dir(path), fs)
}

var digest = &DigestAuth{nil, nil, "", []string{"auth", "auth-int"}, 1, ""}
var basic = &BasicAuthorization{nil, ""}

func (a *AccessFile) ToSchemeHandler(fs FileSystem) (AuthSchemeHandler, error) {
	switch a.Type {
	case Basic:
		basic.realm = a.Realm
		basic.credentials = a.Credentials
		return basic, nil
	case Digest:
		digest.credentials = a.Credentials
		digest.realm = a.Realm
		digest.fs = fs
		return digest, nil
	}
	return nil, errors.New("invalid scheme handler")
}

type Credentials map[string]interface{}

type AuthSchemeHandler interface {
	Authorize(scheme AuthScheme, credentials string, r *Request, w *Response) bool
	WWWAuthenticate(r *Request) string
}

func updateResponseUnauthorized(h AuthSchemeHandler, w *Response, r *Request) {
	updateErrorResponse(w, StatusUnauthorized)
	w.Header.Set(HeaderWWWAuthenticate, h.WWWAuthenticate(r))
	logMain.Debug(w.Header[HeaderWWWAuthenticate])
}

type AccessHandler struct {
	Root                  Dir    `mapstructure:"root"`
	AuthorizationFileName string `mapstructure:"auth_file"`
}

func (a *AccessHandler) FindAccessFile(w *Response, r *Request) (*AccessFile, error) {
	return a.findAccessFile(r.URL.Path, a.Root)
}

func (a *AccessHandler) IsAuthorized(file *AccessFile, err error, w *Response, r *Request) bool {
	if errors.Is(err, NoAuthorizationFileFound) {
		return true
	}

	if err != nil {
		return false
	}

	handler, err := file.ToSchemeHandler(a.Root)
	if err != nil {
		logMain.Error(err)
		return false
	}

	scheme, credentials, err := r.Authorization()
	os.Setenv(AUTH_TYPE, string(scheme))
	if err != nil {
		if isBadRequestError(err) {
			updateErrorResponse(w, StatusBadRequest)
		} else {
			updateResponseUnauthorized(handler, w, r)
		}
		return false
	}

	if !handler.Authorize(scheme, credentials, r, w) {
		updateResponseUnauthorized(handler, w, r)
		return false
	}

	return true
}

func (a *AccessFile) GetAllowedMethods() []string {
	methods := append(make([]string, 0), a.Methods...)
	methods = append(methods, globalConfig.PermittedMethods...)

	return methods
}

func validateAuthScheme(v string) (AuthScheme, error) {
	v = strings.Title(strings.ToLower(v))
	switch v {
	case string(Basic):
		return Basic, nil
	case string(Digest):
		return Digest, nil
	}
	return "", errors.New("no auth scheme")
}

func validateAllowMethod(m string) (string, error) {
	if !validMethod(m) {
		return "", errors.New("invalid method")
	}
	return m, nil

}
