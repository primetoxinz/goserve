package main

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"compress/zlib"
	"fmt"
	"io"
	"strconv"
)

type Response struct {
	Status          string
	StatusCode      int
	Header          Header
	Protocol        string
	ProtocolVersion string
	Request         *Request
	Body            *io.ReadWriter
	ContentLength   int64
}

func (r *Response) writeStatusLine(writer io.Writer) (err error) {
	_, err = fmt.Fprintf(writer, "%s %d %s\r\n", r.Protocol, r.StatusCode, r.Status)
	return err
}

func (r *Response) readBody() ([]byte, error) {
	body := *r.Body
	buf := make([]byte, r.ContentLength)
	_, err := body.Read(buf)
	if err != nil {
		return nil, err
	}
	return buf, nil
}

func (r *Response) writeResponse(writer io.Writer) (err error) {
	err = r.writeStatusLine(writer)
	if err != nil {
		return err
	}
	r.Header.Set(HeaderContentLength, strconv.FormatInt(r.ContentLength, 10))

	bodyWriter := r.getBodyWriter(writer)
	err = r.Header.Write(writer)
	if err != nil {
		return err
	}

	err = CRLF(writer)
	if err != nil {
		return err
	}

	if r.ContentLength != 0 && r.Body != nil && (*r.Body) != nil {
		buf, err := r.readBody()
		if err != nil {
			logMain.Error("Read Body:", err)
		}

		_, err = bodyWriter.Write(buf)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *Response) write(writer io.Writer) (err error) {
	err = r.writeResponse(writer)
	if err != nil {
		return err
	}
	return nil
}

func (r *Response) getBodyWriter(w io.Writer) io.Writer {
	var writer = w
	contentEncoding, ok := r.Header.Get(HeaderContentEncoding)
	if ok {
		switch contentEncoding {
		case "compress":
			writer = zlib.NewWriter(writer)
			break
		case "gzip":
			writer = gzip.NewWriter(writer)
			break
		}
	}

	transferEncoding, ok := r.Header.Get(HeaderTransferEncoding)
	if !ok {
		return writer
	}
	switch transferEncoding {
	case "chunked":
		r.Header.Delete(HeaderContentLength)
		return NewChunkedWriter(writer)
	default:
		return writer
	}
}

func (r *Response) Write(w *bufio.Writer) (err error) {
	if r.Request != nil && r.Request.Method == MethodHead {
		r.Body = nil
	}

	err = r.writeResponse(w)
	if err != nil {
		return err
	}

	err = w.Flush()
	if err != nil {
		return err
	}

	return nil
}

func (r *Response) SetStatus(statusCode int) {
	r.StatusCode = statusCode
	r.Status = StatusText(statusCode)
}

func createResponse(statusCode int, header Header, request *Request, body io.ReadWriter, contentLength int64) *Response {
	if request != nil {
		header.ForwardField(HeaderConnection, request.Header)
		header.ForwardField(HeaderTransferEncoding, request.Header)
	}
	return &Response{StatusText(statusCode), statusCode, header, "HTTP/1.1", "1.1", request, &body, contentLength}
}

func updateErrorResponse(response *Response, statusCode int) {
	response.SetStatus(statusCode)
	body := bytes.NewBufferString("<html><p>" + StatusText(statusCode) + "</p></html>")
	var b io.ReadWriter = body
	response.Body = &b
	response.ContentLength = int64(body.Len())
	response.Header.Set(HeaderContentType, "text/html")
	response.Header.Set(HeaderTransferEncoding, HeaderTransferEncodingChunked)
}

func createErrorResponse(statusCode int, header Header, request *Request) *Response {
	resp := createResponse(statusCode, header, request, nil, 0)
	updateErrorResponse(resp, statusCode)
	return resp
}
