package main

import (
	"errors"
	"fmt"
	"path"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type DigestAuth struct {
	fs          FileSystem
	credentials Credentials
	realm       string
	qop         []string
	nc          int

	lastNonce string
}

var (
	PatternValues = map[string]*regexp.Regexp{
		"username":  regexp.MustCompile("^username=\"(.*)\"$"),
		"realm":     PatternRealm,
		"algorithm": regexp.MustCompile("^algorithm=\"(.*)\"$"),
		"uri":       regexp.MustCompile("^uri=\"(.*)\"$"),
		"qop":       regexp.MustCompile("^qop=(.*)$"),
		"nonce":     regexp.MustCompile("^nonce=\"(.*)\"$"),
		"cnonce":    regexp.MustCompile("^cnonce=\"(.*)\"$"),
		"response":  regexp.MustCompile("^response=\"(.*)\"$"),
		"nc":        regexp.MustCompile("^nc=(\\d+)$"),
	}
)

func submatch(line string, r *regexp.Regexp) (string, error) {
	sub := r.FindStringSubmatch(line)
	if len(sub) != 2 {
		return "", errors.New("invalid digest credential")
	}
	return sub[1], nil
}

func (b *DigestAuth) decodeCredentials(credentials string) (Credentials, error) {
	creds := make(Credentials)

	lines := strings.Split(credentials, ",")
	for _, line := range lines {
		line := strings.TrimSpace(line)
		for key, pattern := range PatternValues {
			if pattern.MatchString(line) {
				s, err := submatch(line, pattern)
				if err != nil {
					return nil, err
				}
				if key == "nc" {
					creds[key], _ = strconv.Atoi(s)
				} else {
					creds[key] = s
				}
			}
		}
	}

	return creds, nil
}

func (b *DigestAuth) generateNonce(etag string) string {
	t := time.Now().UnixNano()
	nonceHash := MD5(fmt.Sprintf("%d:%s,%s", t, etag, globalConfig.DigestSettings.Nonce))
	nonce := MD5(Base64(fmt.Sprintf("%d %s", t, nonceHash)))

	return nonce
}

func (b *DigestAuth) generateOpaque(uri string) string {
	return MD5(fmt.Sprintf("%s:%s", uri, globalConfig.DigestSettings.Opaque))
}

func (b *DigestAuth) desiredHA1(algo, username, nonce, cnonce string) string {
	algo = strings.ToLower(algo)
	user, _ := b.credentials[username].(map[string]string)
	password, _ := user["password"]
	switch algo {
	case "md5":
		return password
	case "md5-sess":
		return MD5(fmt.Sprintf("%s:%s:%s", password, nonce, cnonce))
	}
	return ""
}

func (b *DigestAuth) desiredHA2(qop, method, uri string) string {
	switch qop {
	case "auth":
		return MD5(fmt.Sprintf("%s:%s", method, uri))
	case "auth-int":
		panic("can't do this yet")
	}
	panic("wot")
}

func (b *DigestAuth) Authorize(scheme AuthScheme, credentials string, r *Request, w *Response) bool {
	cred, err := b.decodeCredentials(credentials)
	if err != nil {
		return true
	}

	username, ok := cred["username"].(string)
	if ok {
		info := b.credentials[username].(map[string]string)

		if info["realm"] != cred["realm"] {
			logMain.Debug("Mismatched realm of ", cred["realm"], " for ", info["realm"])
			return false
		}

		algo, ok := cred["algorithm"].(string)
		if !ok {
			algo = "md5"
		}

		nonce, _ := cred["nonce"].(string)
		cnonce, _ := cred["cnonce"].(string)
		nc, _ := cred["nc"].(int)
		qop, _ := cred["qop"].(string)
		uri, _ := cred["uri"].(string)

		if nonce != b.lastNonce {
			b.nc = 0
		}
		b.nc++

		if b.nc != nc {
			return false
		}

		logMain.Debug("NCOUNT:", b.lastNonce, ":", nonce, ":", b.nc)

		HA1 := b.desiredHA1(algo, username, nonce, cnonce)
		HA2 := b.desiredHA2(qop, r.Method, uri)

		responseStr := fmt.Sprintf("%s:%s:%08d:%s:%s:%s", HA1, nonce, b.nc, cnonce, qop, HA2)
		serverResponse := MD5(responseStr)

		RA2 := b.desiredHA2(qop, "", uri)
		rspauth := MD5(fmt.Sprintf("%s:%s:%08d:%s:%s:%s", HA1, nonce, nc, cnonce, qop, RA2))

		response, _ := cred["response"]

		b.lastNonce = nonce
		if serverResponse != response {
			return false
		}

		logMain.Info(response, " ", serverResponse)

		logMain.Info(HA1, ",", HA2)

		b.AuthenticationInfoHeader(rspauth, cnonce, "", w)
		r.User = username
		return true
	}

	return false
}

func (b *DigestAuth) WWWAuthenticate(r *Request) string {
	upath := r.URL.Path
	name := path.Clean(upath)
	etag := ""
	file, err := b.fs.Open(name)
	if err == nil {
		stat, _ := file.Stat()
		etag = strongETag(stat)
	}

	nonce := b.generateNonce(etag)
	opaque := b.generateOpaque(r.RequestURI)
	return fmt.Sprintf(`%s realm="%s",qop="%s",nonce="%s",oqaque="%s",nc="%08d"`,
		Digest,
		b.realm,
		strings.Join(b.qop, ","),
		nonce,
		opaque,
		b.nc,
	)
}

func (b *DigestAuth) AuthenticationInfoHeader(rspauth, cnonce, etag string, w *Response) {
	nonce := b.generateNonce(etag)
	w.Header.Set(HeaderAuthenticationInfo, fmt.Sprintf(`nextnonce="%s",qop="%s",nc="%08d",cnonce="%s",rspauth="%s"`, nonce, strings.Join(b.qop, ","), b.nc, cnonce, rspauth))
	logMain.Info(w.Header[HeaderAuthenticationInfo])
}
