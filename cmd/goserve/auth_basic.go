package main

import (
	"encoding/base64"
	"errors"
	"fmt"
	"strings"
)

type BasicAuthorization struct {
	credentials Credentials
	realm       string
}

func (b *BasicAuthorization) decodeCredentials(credential string) (Credentials, error) {
	decoded, err := base64.StdEncoding.DecodeString(credential)
	if err != nil {
		return nil, err
	}

	d := string(decoded)
	//Find last colon, in case there are other colons in the string
	i := strings.LastIndexFunc(d, func(r rune) bool { return r == ':' })

	if i == -1 || i > len(d) {
		return nil, errors.New("invalid credential encoding")
	}

	cred := make(Credentials)
	cred["username"] = d[:i]
	cred["password"] = d[i+1:]
	return cred, nil
}
func (b *BasicAuthorization) Authorize(scheme AuthScheme, credentials string, r *Request, w *Response) bool {
	cred, err := b.decodeCredentials(credentials)
	if err != nil {
		logMain.Info("credential base64 decoding failed")
		return false
	}
	username, ok := cred["username"].(string)
	if ok {
		passwordHash, ok1 := b.credentials[username]
		if ok1 {
			password, _ := cred["password"].(string)
			if passwordHash == MD5(password) {
				r.User = username
				return true
			}

		}
	}
	return false
}

func (b *BasicAuthorization) WWWAuthenticate(*Request) string {
	return fmt.Sprintf("%s realm=\"%s\"", Basic, b.realm)
}
