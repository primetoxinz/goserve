package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"golang.org/x/net/http/httpguts"
	"io"
	"mime/multipart"
	"net/textproto"
	"net/url"
	"strconv"
	"strings"
)

type Request struct {
	Method          string
	RequestURI      string
	Protocol        string
	ProtocolVersion string
	Host            string
	URL             *url.URL
	Header          Header
	Body            *io.Reader
	ContentLength   int64
	User            string
	MultipartForm   *multipart.Form
}

func (r *Request) ToString() string {
	return fmt.Sprintf("%s %s %s\n%s\n", r.Method, r.RequestURI, r.Protocol, r.Header)
}

func (r *Request) IsSupportedVersion() bool {
	return r.ProtocolVersion == globalConfig.HTTPVersion
}

func (r *Request) ShouldContinueConnection() bool {
	value, _ := r.Header.Get(HeaderConnection)
	return strings.ToLower(value) != HeaderConnectionClose
}

func (r *Request) Authorization() (scheme AuthScheme, credential string, err error) {
	if header, ok := r.Header["Authorization"]; ok {
		logMain.Info(header)
		if len(header) > 1 {
			return "", "", &badRequestError{"multiple authorization headers"}
		}
		auth := header[0]

		i := strings.IndexRune(auth, ' ')
		scheme := auth[:i]

		authScheme, err := validateAuthScheme(scheme)
		if err != nil {
			return "", "", err
		}
		return authScheme, auth[i+1:], nil
	}
	return "", "", errors.New("no authorization header")
}

func (r *Request) WriteRequest(buf *bufio.Writer) {

	_, err := fmt.Fprintf(buf, "%s %s %s\r\n", r.Method, r.RequestURI, r.Protocol)
	if err != nil {
		logMain.Error(err)
	}

	for field, value := range r.Header {
		_, err := fmt.Fprintf(buf, "%s: %s\r\n", field, strings.Join(value, " "))
		if err != nil {
			logMain.Error(err)
		}
	}
	_, err = buf.WriteString("\r\n")
	if err != nil {
		logMain.Error(err)
	}
	if r.Body != nil {
		body := *r.Body
		bodyBuf := make([]byte, r.ContentLength)
		_, err = body.Read(bodyBuf)
		if err != nil {
			logMain.Error(err)
		}
		_, _ = buf.Write(bodyBuf)
	}
	_ = buf.Flush()
}

func readRequest(buf *bufio.Reader) (request *Request, err error) {

	tp := textproto.NewReader(buf)
	request = new(Request)
	var line string
	if line, err = tp.ReadLine(); err != nil {
		logMain.Debug("Read Line:", err)
		return nil, err
	}

	logMain.Debug("Request line:", line)
	var okRequestLine bool
	var okProtocol bool
	//Read all the parts
	//Validate afterwards
	request.Method, request.RequestURI, request.Protocol, okRequestLine = parseRequestLine(line)
	request.ProtocolVersion, okProtocol = parseProtocol(request.Protocol)
	mimeHeader, err := tp.ReadMIMEHeader()

	if !okRequestLine {
		logMain.Debug("Failed to parse request line:", line)
		return nil, &badRequestError{StatusText(StatusBadRequest)}
	}
	logMain.Debug("Method was valid:", request.Method)

	if !okProtocol {
		logMain.Debug("Failed to parse protocol")
		return nil, &badRequestError{StatusText(StatusBadRequest)}
	}
	logMain.Debug("Protocol was valid:", request.Protocol)

	if !request.IsSupportedVersion() {
		logMain.Debug("Version is not supported ", request.Protocol)
		return nil, &unsupportedVersionError{StatusText(StatusHTTPVersionNotSupported)}
	}
	logMain.Debug("Version was supported:", request.ProtocolVersion)

	if err != nil {
		logMain.Debug(err)
		return nil, &badRequestError{err.Error()}
	}

	request.Header = Header(mimeHeader)
	if request.Host == "" {
		request.Host, _ = request.Header.Get("Host")
	}

	err = validHost(request)
	if err != nil {
		logMain.Debug("Invalid Host header field: ", request.Host)
		return nil, err
	}

	if request.URL, err = url.ParseRequestURI(request.RequestURI); err != nil {
		return nil, err
	}

	upath := request.URL.Path
	//Ensure request URL Path begins with /
	if upath != "" && !strings.HasPrefix(upath, "/") {
		upath = "/" + upath
		request.URL.Path = upath
	}

	logMain.Debug("Host was valid:", request.Host)

	if !validMethod(request.Method) {
		logMain.Debug("Invalid method", request.Method)
		return nil, &invalidMethodError{request.Method}
	}

	if err = readRequestBody(request, buf); err != nil {
		return nil, err
	}

	return request, nil
}

func readRequestBody(r *Request, buf *bufio.Reader) error {
	if l, ok := r.Header.Get(HeaderContentLength); ok {

		length, err := strconv.Atoi(l)
		if err != nil {
			return err
		}
		r.ContentLength = int64(length)
		b := make([]byte, length)
		_, err = buf.Read(b)
		if err != nil {
			return err
		}
		var b2 io.Reader = bytes.NewBuffer(b)
		r.Body = &b2
	}

	return nil
}

func validHost(request *Request) error {
	header := request.Header
	hosts, ok := header["Host"]

	if !ok || len(hosts) == 0 {
		return &badRequestError{"missing host header"}
	}

	//Can't have multiple hosts and urls can't have whitespace.
	if len(hosts) > 1 {
		return &badRequestError{"too many hosts"}
	}

	//Use httpguts because I have no intention of writing this insane method
	if len(hosts) == 1 && !httpguts.ValidHostHeader(hosts[0]) {
		return &badRequestError{"malformed Host header"}
	}

	return nil
}

func validMethod(method string) bool {
	return contains(globalConfig.SupportedMethods, method)
}

func parseProtocol(protocol string) (version string, ok bool) {
	prefix := "HTTP/"
	if !strings.HasPrefix(protocol, prefix) {
		return "", false
	}
	return strings.TrimPrefix(protocol, prefix), true
}

func parseRequestLine(line string) (method, requestUri, proto string, ok bool) {
	s1 := strings.Index(line, " ")
	s2 := strings.Index(line[s1+1:], " ")
	if s1 < 0 || s2 < 0 {
		return
	}
	s2 += s1 + 1
	return line[:s1], line[s1+1 : s2], line[s2+1:], true
}
