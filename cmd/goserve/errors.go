package main

import (
	"io"
	"net"
)

func handleError(err error, f func(err error) bool) bool {
	if err != nil {
		return f(err)
	}
	return false
}

type invalidMethodError struct {
	err string
}

func (e *invalidMethodError) Error() string {
	return e.err
}

func isInvalidMethod(err error) bool {
	_, ok := err.(*invalidMethodError)
	return ok
}

type unsupportedVersionError struct {
	err string
}

func (e *unsupportedVersionError) Error() string {
	return e.err
}

func isUnsupportedVersionError(err error) bool {
	_, ok := err.(*unsupportedVersionError)
	return ok
}

type badRequestError struct {
	err string
}

func (e *badRequestError) Error() string {
	return e.err
}

func isBadRequestError(err error) bool {
	_, ok := err.(*badRequestError)
	return ok
}

func isConnectionClosed(err error) bool {
	if err == io.EOF {
		return true
	}
	if neterr, ok := err.(net.Error); ok && neterr.Timeout() {
		return true
	}
	return false
}

type badRangeError string

func (e badRangeError) Error() string {
	return string(e)
}

func isBadRange(err error) bool {
	_, ok := err.(badRangeError)
	return ok
}

type invalidContentSpec string

func (e invalidContentSpec) Error() string {
	return string(e)
}

type unsatisfiableRangeSet string

func (e unsatisfiableRangeSet) Error() string {
	return string(e)
}

func isUnsatisfiableRangeSet(err error) bool {
	_, ok := err.(unsatisfiableRangeSet)
	return ok
}
