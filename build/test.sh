#!/bin/bash

function getSummary() {
  file="$1"
  line=$(tac $file | grep -n SUMMARY | cut -d : -f 1)
#  echo $line
  line=$(( $line - 2 ))
#  echo $line
  tail -n $line $file
}

FILE="$GITHUB_WORKSPACE/webserver-tester/main.py"

if [ -f $FILE ]; then
  python $FILE "$@" > results.txt 2>&1
  sed -i "s/[=-]//g" results.txt
  full_results=$(cat results.txt)
  summary=$(getSummary results.txt)

  echo $full_results

fi