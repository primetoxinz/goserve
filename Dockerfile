FROM golang:1.13
RUN mkdir -p /go/src/github.com/primetoxinz/web-server/cmd/goserve
RUN mkdir -p /var/www/
RUN go get -u github.com/golang/dep/cmd/dep
COPY ./build/run.sh /run.sh
WORKDIR /go/src/github.com/primetoxinz/web-server/cmd/goserve
COPY ./Gopkg.toml Gopkg.toml
COPY ./cmd/goserve ./
RUN dep ensure
#RUN go test -v
RUN go build -o /usr/bin/webserver
COPY ./configs ./configs
EXPOSE 80
CMD ["/run.sh"]

