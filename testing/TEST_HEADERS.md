GET /hello.txt HTTP/1.1
User-Agent: curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3
Host: www.example.com
Accept-Language: en, mi


OPTIONS /hello.txt HTTP/1.1
User-Agent: curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3
Host: www.example.com
Accept-Language: en, mi


POST /hello.txt HTTP/1.1
User-Agent: curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3
Host: www.example.com
Accept-Language: en, mi

GET /hello.txt HTTP/1.2

GET /hello.txt HTTP/1.1
Host: www.example.com
Connection: keep-alive

GET /hello.txt HTTP/1.1
Host: www.example.com
Connection: keep-alive

GET /hello.txt HTTP/1.1
Host: www.example.com
Connection: keep-alive